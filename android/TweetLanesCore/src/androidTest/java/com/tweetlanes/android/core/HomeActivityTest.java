package com.tweetlanes.android.core;

import android.test.ActivityInstrumentationTestCase2;


import com.robotium.solo.By;
import com.robotium.solo.Solo;
import com.tweetlanes.android.core.view.HomeActivity;
import com.tweetlanes.android.core.widget.EditClearText;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HomeActivityTest extends ActivityInstrumentationTestCase2<HomeActivity> {

    private Solo solo;

    public HomeActivityTest() throws ClassNotFoundException {
        super(HomeActivity.class);
    }

    @Before
    public void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
        assertNotNull(solo);
    }


//    @Test
//    public void testTwitterAcceptButtonClicked() throws Exception {
//        solo.scrollDown();
//        By acceptBtn = By.id("acceptBtn"); // tutorial_thanks.xml
//        //solo.getView(R.id.acceptBtn);
//        solo.clickOnButton(acceptBtn.toString());
//        Assert.assertTrue(solo.searchText("Change log "));
//        solo.sleep(5000);
//    }


//    @Test
//    public void testHomeActivityCompostTweet() throws Exception {
////      solo.waitForWebElement(By.tagName("DIV"));
////      EditText composeTweet = (EditText) findById("composeTweetFragment");
//        By acceptBtn = By.id("acceptBtn");
//        solo.clickOnButton(acceptBtn.toString());
////      By statusEditText = By.id("statusEditText");
////      solo.clickOnEditText(statusEditText);
////      solo.typeText( statusEditText, "Hi");
////      solo.typeText(statusEditText, "Nice beautiful day");
//        solo.sleep(5000);
//    }


    @After
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }
}
