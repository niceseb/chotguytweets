package com.tweetlanes.android.core;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import com.robotium.solo.By;
import com.robotium.solo.Solo;
import com.robotium.solo.WebElement;
import com.tweetlanes.android.core.view.TwitterAuthActivity;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

//import java.lang.annotation.TargetApi;


//@TargetApi(22)
public class TwitterAuthActivityTest extends ActivityInstrumentationTestCase2<TwitterAuthActivity>{

	private Solo solo;
	private static final String LOG_TAG = "debugger";
	private Intent mStartIntent;

	public TwitterAuthActivityTest() throws ClassNotFoundException {
		super(TwitterAuthActivity.class);
	}

	@Before
	public void setUp() throws Exception {
		//setUp() is run before a test case is started.
		//This is where the solo object is created.
		solo = new Solo(getInstrumentation(), getActivity());
		assertNotNull(solo);
	}

	@Test
	public void testTwitterAuthLogin() throws Exception {
		//solo.setActivityOrientation(Solo.LANDSCAPE);
		solo.scrollToTop();
		solo.waitForWebElement(By.tagName("DIV"));
		By username = By.id("username_or_email");
		By password = By.id("password");
		By allow = By.id("allow");
 
		logWebElementsFound();

		solo.typeTextInWebElement(username, "dummy");  //Type in your Twitter UserName "Your username"
		solo.typeTextInWebElement(password, "dummy");//Type in your Twitter Password "Your password"
		solo.clickOnWebElement(allow);

		solo.getView(R.id.bioTextView); // tutorial_welcome.xml Welcome to ChotguyTweets message
		Assert.assertTrue(solo.searchText("Welcome to "));

		solo.sleep(5000);
	}

	/**
	 * Logs the WebElements currently displayed.
	 */
	private void logWebElementsFound(){
		for(WebElement webElement : solo.getCurrentWebElements()){
			Log.d("Robotium", "id: '" + webElement.getId() + "' text: '" + webElement.getText() + "' name: '" + webElement.getName() +
					"' class name: '" + webElement.getClassName() + "' tag name: '" + webElement.getTagName() + "'");
		}
	}

	@Test
	public void testTwitterAuthCancelBtnPressed() throws Exception {

		solo.waitForWebElement(By.tagName("DIV"));
		By cancel = By.id("cancel");

		solo.clickOnWebElement(cancel);
		solo.waitForWebElement(By.className("action-information"));
		Assert.assertTrue(solo.searchText("You have not signed in to "));
		solo.sleep(1000);
	}

	@After
	public void tearDown() throws Exception {

		solo.finishOpenedActivities();

	}
}