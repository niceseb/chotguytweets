package com.tweetlanes.android.core;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;

import junit.framework.TestSuite;

import org.junit.After;
import org.junit.Before;

public class AllTests extends ActivityInstrumentationTestCase2<Activity> {


    public AllTests(Class<Activity> activityClass) throws ClassNotFoundException {
        super(activityClass);
    }

    public static TestSuite suite() {
        TestSuite t = new TestSuite();

        t.addTestSuite(TwitterAuthActivityTest.class);
//        t.addTestSuite(HomeActivityTest.class);

        return t;
    }

    @Before
    public void setUp()  throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
}
